package com.company.task3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TimeTest {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    System.out.println("\n___________________TEST______________________\n");
    System.out.println("Pleae enter number of objects to add\n");
    int input = in.nextInt();

    long startTimeMy = System.currentTimeMillis();

    MyArrayList<String> myArrayList = new MyArrayList<>();

    for (int i = 0; i < input; ++i) {
      myArrayList.add("hello");
    }

    long stopTimeMy = System.currentTimeMillis();
    long elapsedTimeMy = stopTimeMy - startTimeMy;
    System.out.println("\n__________________RESULT_____________________\n");
    System.out.println("My time is " + elapsedTimeMy + " milliseconds on "+ myArrayList.size()+ " objects\n");

    long startTime = System.currentTimeMillis();

    List<String> list = new ArrayList<>();

    for(int i = 0; i< input; ++i){
      list.add("hello");
    }

    long stopTime = System.currentTimeMillis();
    long elapsedTime = stopTime - startTime;
    System.out.println("ArrayList time is "+elapsedTime+ " milliseconds on "+list.size()+" objects");
  }

}
