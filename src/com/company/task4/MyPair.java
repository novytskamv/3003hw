package com.company.task4;

import com.company.task3.MyArrayList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MyPair {

  private String s1;
  private String s2;

  public MyPair(String s1, String s2){
    this.s1 = s1;
    this.s2 = s2;
  }

  static Comparator<MyPair> getCountryComparator() {
    return new Comparator<MyPair>() {
      @Override
      public int compare(MyPair myPair, MyPair t1) {
        return myPair.s1.compareTo(t1.s1);
      }
    };
  }

  static Comparator<MyPair> getCapitalComparator() {
    return new Comparator<MyPair>() {
      @Override
      public int compare(MyPair myPair, MyPair t1) {
        return myPair.s2.compareTo(t1.s2);
      }
    };
  }

  @Override
  public String toString() {
    return "MyPair{" +
        "s1='" + s1 + '\'' +
        ", s2='" + s2 + '\'' +
        '}'+ "\n";
  }

  public static void main(String[] args) {

    MyArrayList<MyPair> myPair = new MyArrayList<>();
    Generator generator = new Generator();

    for(int i =0; i< 10; ++i){
      myPair.add(generator.next());
    }

    Collections.sort(myPair, getCountryComparator());
    System.out.println(myPair);

    ArrayList<MyPair> myPair1 = new ArrayList<>();
    Generator generator1 = new Generator();

    for(int i =0; i< 10; ++i){
      myPair1.add(generator1.next());
    }
    System.out.println("__________SORTING_________\n");

    Collections.sort(myPair1, MyPair.getCountryComparator());

    System.out.println("Sorting by country name: \n"+myPair1);

    Collections.sort(myPair1, MyPair.getCapitalComparator());

    System.out.println("Sorting by capital: \n"+myPair1);


  }


}
