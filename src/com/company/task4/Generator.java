package com.company.task4;

public class Generator {


  private static MyPair p[] = new MyPair[]{
      new MyPair("Ukraine", "Kyiv"),
      new MyPair("France", "Paris"),
      new MyPair("Germany", "Berlin"),
      new MyPair("Georgia", "Tbilisi"),
      new MyPair("Andora", "Andora")
  };

  private int i=0;

  public MyPair next(){
    MyPair r = p[i];
    i=(i+1)%p.length;
    return r;
  }

}
