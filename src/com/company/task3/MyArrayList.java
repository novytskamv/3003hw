package com.company.task3;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import jdk.nashorn.internal.runtime.arrays.ArrayLikeIterator;

public class MyArrayList<T> extends AbstractList<T> implements Iterable<T>{

  private static final int INITIAL_CAPACITY = 16;
  private int size = 0;
  private T[] elements;

  public MyArrayList() {
    this.elements = (T[]) new Object[INITIAL_CAPACITY];
  }

  public boolean add(T element) {
    if (size == elements.length) {
      ensureCapacity();
    }
    elements[size++] = element;
    return true;
  }

  public T get(int index){
    return elements[index];
  }

  public T set(int index, T element){
    T oldValue = elements[index];
    elements[index] = element;
    return oldValue;
  }

  private void ensureCapacity() {
    int newSize = elements.length * 2;
    elements = Arrays.copyOf(elements, newSize);
  }

  public int size(){
    return size;
  }

  @Override
  public Iterator<T> iterator() {
    return new Iterator<T>() {
      int current = 0;

      @Override
      public boolean hasNext() {
        if(current<size){
          return true;
        }else{
          return false;
        }
      }

      @Override
      public T next() {
        if(!hasNext()){
          throw new NoSuchElementException("No next elements");
        }
        return elements[current++];
      }
    };
  }

  public static void main(String[] args) {

    MyArrayList<Integer> a = new MyArrayList<>();
    assert a.size() == 0;

    a.add(1);
    a.add(2);
    a.add(3);

    assert a.size() == 3;

  }

}
