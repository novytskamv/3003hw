package com.company.task5;

import java.util.Arrays;

public class MyDeque<T> {

  private T[] elements;
  private int sizeEnd = 0;
  private int sizeBegin = 0;

  private static final int DEFAULT_CAPACITY = 16;

  public MyDeque() {
    this.elements = (T[])new Object[DEFAULT_CAPACITY];
  }

  public int size() {
    return sizeEnd + sizeBegin;
  }

  public void addEnd(T element) {
    if (this.sizeEnd == elements.length) {
      ensureCapacity();
    }
    elements[sizeEnd++] = element;
  }


  public void addBegin(T element) {
    if (this.sizeEnd == elements.length) {
      ensureCapacity();
    }
    int tmpSize = sizeEnd+1;
    while (tmpSize>0){
      elements[tmpSize]=elements[tmpSize-1];
      tmpSize--;
    }
    elements[0]=element;
    sizeEnd++;
  }

  public T getEnd() {
    return elements[sizeEnd-1];
  }

  public T getBegin() {
    return elements[0];
  }

  //2 variant

  public void addBegin2(T element) {
    if (this.sizeBegin == elements.length) {
      ensureCapacity();
    }
    elements[(elements.length / 2) + (--this.sizeBegin)] = element;
  }

    public void addEnd2(T element) {
    if (this.sizeEnd == elements.length) {
      ensureCapacity();
    }
    elements[(elements.length / 2) + (this.sizeEnd++)] = element;
  }


  public Object getEnd2() {
    return elements[elements.length / 2 + this.sizeEnd -1];
  }

  public Object getBegin2() {
    return elements[elements.length / 2 + this.sizeBegin];
  }


  public void ensureCapacity() {
    int newSize = elements.length * 2;
    elements = Arrays.copyOf(elements, newSize);
  }

//  @Override
//  public String toString() {
//    T [] el2 = (T[]) new Object[size()];
//    for(int i = 0; i<size(); i++)
//      el2[i] = elements[i];
//    return Arrays.toString(el2);
//  }

  @Override
  public String toString() {
    String result = "[";
    String delim = "";
    for(int i = 0; i<size(); i++) {
      if (elements[i] != null)
        result += delim + elements[i];
      delim = ", ";
    }
    result += "]";
    return result;
  }


  public static void main(String[] args) {

    MyDeque<Integer> myDeque = new MyDeque<>();

    myDeque.addBegin(5);
    System.out.println("Let see my current deque: \n"+myDeque);
    myDeque.addEnd(6);
    System.out.println("Let see my current deque: \n"+myDeque);
    myDeque.addBegin(2);
    System.out.println("Let see my current deque: \n"+myDeque);
    myDeque.addEnd(7);
    System.out.println("Let see my current deque: \n"+myDeque);
    myDeque.addEnd(5);
    System.out.println("Let see my current deque: \n"+myDeque);
    myDeque.addEnd(8);
    System.out.println("Let see my current deque: \n"+myDeque);
    myDeque.addBegin(14);


    System.out.println("Let see my current deque: \n"+myDeque);
    System.out.println( "\nThe last element of MyDeque is \n"+myDeque.getEnd());
    System.out.println("\nThe first element of MyDeque is \n"+myDeque.getBegin());

  }

}
